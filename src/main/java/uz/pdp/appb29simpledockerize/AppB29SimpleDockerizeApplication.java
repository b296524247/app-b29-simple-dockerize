package uz.pdp.appb29simpledockerize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class AppB29SimpleDockerizeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppB29SimpleDockerizeApplication.class, args);
    }


    @GetMapping("/hello")
    public String message(@RequestParam(required = false, defaultValue = "Spring") String name) {
        return "Hello from Docker V2 to " + name;
    }
}
